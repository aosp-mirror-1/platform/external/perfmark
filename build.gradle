plugins {
    id "net.ltgt.errorprone" version "3.0.1" apply false
    id "me.champeau.jmh" version "0.6.8" apply false
    id "io.github.reyerizo.gradle.jcstress" version "0.8.14" apply false
}

subprojects {
    apply plugin: "checkstyle"
    apply plugin: "java-library"
    apply plugin: 'maven-publish'
    apply plugin: "idea"
    apply plugin: "signing"
    apply plugin: "net.ltgt.errorprone"

    repositories {
        maven {
            url "https://maven-central.storage-download.googleapis.com/repos/central/data/" }
        mavenCentral()
    }

    java {
        toolchain {
            languageVersion = JavaLanguageVersion.of(17)
        }
    }
    
    task javadocJar(type: Jar) {
        classifier = 'javadoc'
        from javadoc
    }

    task sourcesJar(type: Jar) {
        classifier = 'sources'
        from sourceSets.main.allSource
    }

    checkstyle {
        configDirectory = file("$rootDir/buildscripts")
        toolVersion = "6.17"
        ignoreFailures = false
        if (rootProject.hasProperty("checkstyle.ignoreFailures")) {
            ignoreFailures = rootProject.properties["checkstyle.ignoreFailures"].toBoolean()
        }
    }

    afterEvaluate {
        jar {
            manifest {
                attributes  ('Automatic-Module-Name': moduleName,
                    "Implementation-Version": archiveVersion.get(),
                    "Implementation-Title": "PerfMark",
                    "Implementation-Vendor": "Carl Mastrangelo",
                    "Implementation-URL": "https://www.perfmark.io/",
                    "Carl-Is-Awesome": "true")
            }

        }
    }


    publishing {
        publications {
            maven(MavenPublication) {
                from components.java

                artifact javadocJar
                artifact sourcesJar

                pom {
                    name = project.group + ":" + project.name
                    url = 'https://github.com/perfmark/perfmark'
                    afterEvaluate {
                        // description is not available until evaluated.
                        description = project.description
                    }

                    scm {
                        connection = 'scm:git:https://github.com/perfmark/perfmark.git'
                        developerConnection = 'scm:git@github.com:perfmark/perfmark.git'
                        url = 'https://github.com/perfmark/perfmark'
                    }

                    licenses {
                        license {
                            name = 'Apache 2.0'
                            url = 'https://opensource.org/licenses/Apache-2.0'
                        }
                    }

                    developers {
                        developer {
                            id = "carl-mastrangelo"
                            name = "Carl Mastrangelo"
                            email = "carl@carlmastrangelo.com"
                            url = "https://www.perfmark.io/"
                        }
                    }
                }
            }
        }

        repositories {
             maven {
                 def stagingUrl = 'https://oss.sonatype.org/service/local/staging/deploy/maven2/'
                 def releaseUrl = stagingUrl
                 def snapshotUrl = 'https://oss.sonatype.org/content/repositories/snapshots/'
                 url = version.endsWith('SNAPSHOT') ? snapshotUrl : releaseUrl
                 credentials {
                     if (rootProject.hasProperty('ossrhUsername')
                             && rootProject.hasProperty('ossrhPassword')) {
                         username = rootProject.ossrhUsername
                         password = rootProject.ossrhPassword
                     }
                 }
            }
        }
    }
    
    signing {
        required false
        sign publishing.publications.maven
    }

    [publishMavenPublicationToMavenRepository, publishMavenPublicationToMavenLocal]*.onlyIf {
        !name.contains("perfmark-examples") && !name.contains("perfmark-api-testing")
                && !name.contains("perfmark-testing") && !name.contains("perfmark-agent")
    }

    [javadoc]*.onlyIf {
        !name.contains("perfmark-java9") && !name.contains("perfmark-examples")
                && !name.contains("perfmark-api-testing") && !name.contains("perfmark-testing")
    }

    if (rootProject.properties.get('errorProne', true)) {
        dependencies {
            errorprone 'com.google.errorprone:error_prone_core:2.16'
            errorproneJavac 'com.google.errorprone:javac:9+181-r4173-1'
        }
    } else {
        // Disable Error Prone
        allprojects {
            afterEvaluate { project ->
                project.tasks.withType(JavaCompile) {
                    options.errorprone.enabled = false
                }
            }
        }
    }

    group = "io.perfmark"
    version = "0.26.0"

    dependencies {
        testImplementation libs.junit
    }
}
